use std::env;

mod global;

enum Token {
    CHARACTER(char),
    PRINT,
    DELETE,
    NEWLINE,
    REVERSE,
}

fn run(input: Vec<Token>) {
    #[cfg(debug_assertions)]
    println!("Running");
    let mut buf: String = "".to_string();
    for token in input {
        match token {
            Token::CHARACTER(c) => { buf.push(c); }
            Token::PRINT => { print!("{buf}"); }
            Token::DELETE => { buf.pop(); }
            Token::NEWLINE => { buf += "\n"; }
            Token::REVERSE => { buf = buf.chars().rev().collect::<String>(); }
        }
    }
}

fn parse(input: &str) -> Vec<Token> {
    #[cfg(debug_assertions)]
    println!("Parsing");
    let mut tokens = Vec::new();
    let mut next_char_is_char = false;
    for char in input.chars() {
        #[cfg(debug_assertions)]
        println!("Parsing char: {char}");
        if !next_char_is_char {
            match char {
                'c' => { next_char_is_char = true }
                'p' => { tokens.push(Token::PRINT) }
                'd' => { tokens.push(Token::DELETE) }
                'n' => { tokens.push(Token::NEWLINE) }
                'r' => { tokens.push(Token::REVERSE) }
                '\n' => {}
                ' ' => {}
                _ => { println!("Unknown symbol: {}", char) }
            }
        } else {
            #[cfg(debug_assertions)]
            println!("Adding char as character: {char}");
            tokens.push(Token::CHARACTER(char));
            next_char_is_char = false
        }
    }
    tokens
}

fn main() {
    println!("{} v{}", global::NAME, global::VERSION);
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);
    if args.len() < 1 {
        println!("At least one file is required as an argument");
        return;
    }
    for file in args {
        #[cfg(debug_assertions)]
        println!("Loading file: {file}");
        if !file.ends_with(".ssspl") {
            #[cfg(debug_assertions)]
            println!("Could not load file, does not end with .ssspl: {file}");
        } else  {
            let input = std::fs::read_to_string(file).expect("Failed to read file {file}");
            run(parse(&input));
        }
    }
}
