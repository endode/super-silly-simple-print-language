use std::fmt::{Debug, Display};

pub struct Version {
    major: u8,
    minor: u8,
    patch: u8,
}

impl Debug for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Version {{ major: {}, minor: {}, patch: {} }}", self.major, self.minor, self.patch)
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

pub const VERSION: Version = Version{major: 1, minor: 1, patch: 0};

#[cfg(not(debug_assertions))]
pub const NAME: &str = "Super Silly Simple Print Language";
#[cfg(debug_assertions)]
pub const NAME: &str = "Super Silly Simple Print Language | Debug";
