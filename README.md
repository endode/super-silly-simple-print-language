# Super Silly Simple Print Language

I made this so I can learn how to parse files, so I can possibly make my own small programming/scripting language one day. Although, this project is probably too simple for that

Hello World in SSSPL; prints "Hello World!\n"
```ssspl
cHceclclcoc cWcocrclcdc!np
```

c means the next character will be added to the string

p means to print the string

d means delete last character

n for newline

r to reverse the buffer
